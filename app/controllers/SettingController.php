<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/29/2018
 * Time: 6:22 AM
 */

class SettingController extends ControllerBase
{

    public function create()
    {

        // captures fields
        $full_name = $this->request->getPost("full_name");
        $setting_value = $this->request->getPost("setting_value");

        if (!$full_name) {

            $message = "Name is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }
        if (!$setting_value) {

            $message = "Setting Value is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }


        $sql = "INSERT INTO setting (full_name,setting_value,created) VALUE (:full_name,:setting_value,now())";

        $params = array(
            ":full_name" => $full_name,
            ":setting_value" => $setting_value
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Settings added successfully";
            return $this->systemResponse($message, "200", "Success");
        } catch (Exception $e) {

            $message = $e->getMessage();
            return $this->systemResponse($message, "500", "Error Occured");
        }
    }

    public function update()
    {

        // captures fields
        $setting_value = $this->request->getPost("setting_value");
        $id = $this->request->getPost("id");

        if (!$id) {

            $message = "ID is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        $updates = array();
        $bindingParams = array();

        if ($setting_value) {

            $updates[] = "setting_value = :setting_value";
            $bindingParams[':setting_value'] = $setting_value;
        }


        if (count($updates) > 0) {

            $sql = "UPDATE setting SET " . implode(',', $updates) . " WHERE id = :id ";
            $bindingParams[':id'] = $id;

            try {

                $dt = $this->rawInsert($sql, $bindingParams);
                $message = "Changes saved successfully";
                return $this->systemResponse($message, "200", "Success");
            } catch (Exception $e) {

                $message = $e->getMessage();
                return $this->systemResponse($message, "500", "Error Occured");
            }

        }

    }

    public function delete()
    {

        // captures fields
        $id = $this->request->getPost("id");

        if (!$id) {

            $message = "ID is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        $sql = "DELETE from setting WHERE id = :id LIMIT 1";
        $params = [':id' => $id];

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Setting deleted successfully";
            return $this->systemResponse($message, "200", "Success");
        } catch (Exception $e) {

            $message = $e->getMessage();
            return $this->systemResponse($message, "500", "Error Occured");
        }

    }

    public function view()
    {

        $json = $this->request->getJsonRawBody();
        // captures fields
        $id = $this->request->getPost("id");
        $full_name = $this->request->getPost("full_name");

        if(!$full_name){

            $full_name = isset($json->full_name) ? $json->full_name : false;
            $id = isset($json->id) ? $json->id : false;

        }

        if($id){

            $sql = "SELECT * FROM setting WHERE id = :id ";
            $params = [':id'=>$id];
        }
        else if($full_name){

            $sql = "SELECT * FROM setting WHERE full_name = :full_name ";
            $params = [':full_name'=>$full_name];
        }
        else {

            $sql = "SELECT * FROM setting";
            $params = null;
        }

        try {

            $dt = $this->rawSelect($sql, $params);
            return $this->systemResponse($dt,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }

}
