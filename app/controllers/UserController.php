<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/29/2018
 * Time: 6:22 AM
 */

class UserController extends ControllerBase
{

    public function create()
    {

        // captures fields
        $full_name = $this->request->getPost("full_name");
        $email = $this->request->getPost("email");
        $password = $this->request->getPost("password");

        // capture JSON raw

        $json = $this->request->getJsonRawBody();
        if (!$full_name) {

            $full_name = $json->full_name;
        }

        if (!$email) {

            $email = $json->email;
        }

        if (!$password) {

            $password = $json->password;
        }

        // validate fields

        if (!$full_name) {

            $message = "Full Name is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        if (!$email) {

            $message = "Email is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        if (!$password) {

            $message = "Password is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        if (strlen($password) < 5) {

            $message = "Password is too short";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        $sql = "INSERT INTO user (full_name,email,password,status,created) VALUE (:fullname,:email,:password,1, now())";

        $params = array(
            ":fullname" => $full_name,
            ":email" => $email,
            ":password" => md5($password)
        );

        try {

            $dt = $this->rawInsert($sql, $params);

            $msg = "Dear $full_name<br><br>Your account at wakobo.com has been created successfullu. To login <a href='www.wakobo.com/login'>Click Here </a> <br><br> <strong> Username: $email<br> Password: $password</strong>";

            $this->sendMail($email, $full_name, "New Account", $msg);

            $message = "user created successfully";
            return $this->systemResponse($message, "200", "Success");
        } catch (Exception $e) {

            $message = $e->getMessage();
            return $this->systemResponse($message, "500", "Error Occured");
        }


    }

    public function changeStatus()
    {

        // captures fields
        $id = $this->request->getPost("id");
        $status = $this->request->getPost("status");

        // validate fields

        if (!$id) {

            $message = "User ID is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        if (!$status) {

            $message = "Status is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        if ($status != 0 || $status != 1) {

            $message = "Wrong Status";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        $sql = "UPDATE user set status = :status WHERE id = :id ";

        $params = array(
            ":status" => $status,
            ":id" => $id
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "user updated successfully";
            return $this->systemResponse($message, "200", "Success");
        } catch (Exception $e) {

            $message = $e->getMessage();
            return $this->systemResponse($message, "500", "Error Occured");
        }
    }

    /**
     * perfom send email
     * @param string $email
     * @param string $name
     * @param string $subject
     * @param string $msg
     * @return array|bool
     */
    public function sendMail($email, $name, $subject, $msg)
    {

        $imgSrc = 'https://southwell.io/img/logo.png';
        $imgDesc = 'Wakobo search logo';
        $imgTitle = 'Wakobo';

        $content = '<!DOCTYPE HTML>' .
            '<head>' .
            '<meta http-equiv="content-type" content="text/html">' .
            '<title>' . $subject . '</title>' .
            '</head>' .
            '<body>' .
            '<div id="header" style="width: 80%;height: 60px;margin: 0 auto;padding: 10px;color: #fff;text-align: center;background-color: #e22b25  ;font-family: Open Sans,Arial,sans-serif;">' .
            '<img height="50" width="220" style="border-width:0" src="' . $imgSrc . '" alt="' . $imgDesc . '" title="' . $imgTitle . '">' .
            '</div>' .
            '<div id="inner" style="width: 78%;margin: 0 auto;background-color: #fff;font-family: Open Sans,Arial,sans-serif;font-size: 13px;font-weight: normal;line-height: 1.4em;color: #444;margin-top: 10px;">' .
            $msg .
            '</div>' .
            '<div id="footer" style="width: 80%;height: 40px;margin: 0 auto;text-align: center;padding: 10px;font-family: Verdena;background-color: #e22b25  ;">' .
            'All rights reserved @ <a href="https://www.southwell.io">; Wakobo 2018 LTD </b>' . date('Y') .
            '</div>' .
            '</body>' .
            '</html>';

        $senderID = $this->config->email->senderID;
        $smtp = $this->config->email->smtp;
        $smtpPort = $this->config->email->smtpPort;
        $username = $this->config->email->username;
        $password = $this->config->email->password;


        $from = array($senderID => 'Wakobo');
        $transport = Swift_SmtpTransport::newInstance($smtp, $smtpPort, 'ssl');
        $transport->setUsername($username);
        $transport->setPassword($password);
        $swift = Swift_Mailer::newInstance($transport);
        $message = new Swift_Message($subject);
        $message->setFrom($from);
        $message->setBody($content, 'text/html');
        $message->setSubject($subject);

        $message->setTo(array($email => $name));

        if ($swift->send($message, $failures)) {
            $this->info("SWIFT MAILER got succss ");
            return true;
        } else {
            $this->info("SWIFT MAILER got error " . json_encode($failures));

            return false;
        }
    }

    public function changePassword()
    {

        // captures fields
        $id = $this->request->getPost("id");
        $old_password = $this->request->getPost("old_password");
        $new_password = $this->request->getPost("new_password");

        // validate fields

        if (!$id) {

            $message = "User ID is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        if (!$old_password) {

            $message = "Old Password is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        if (!$new_password) {

            $message = "New Password is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        if (strlen($new_password) < 5) {

            $message = "New Password is too short";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        $sql = "SELECT id FROM user WHERE id=:id AND password=:password";

        $params = array(
            ":id" => $id,
            ":password" => md5($old_password)
        );

        $data = $this->fetchOne($sql, $params);

        if ($data) {

            $sql = "UPDATE user set password=:password WHERE id=:id";

            $params = array(
                ":id" => $id,
                ":password" => md5($new_password)
            );

            $sql = "SELECT full_name,email FROM user WHERE id = :id";

            $params = array(":id" => $id);

            $user = $this->fetchOne($sql, $params);

            $full_name = $user->full_name;
            $email = $user->email;

            $msg = "Dear $full_name<br><br>Your account password at wakobo.com was changed successfully. To login <a href='www.wakobo.com/login'>Click Here </a> <br><br> <strong> Username: $email<br> New Password: $new_password</strong>";

            $this->sendMail($email, $full_name, "New Account", $msg);

            try {

                $dt = $this->rawInsert($sql, $params);
                $message = "Password changed successfully";
                return $this->systemResponse($message, "200", "Success");
            } catch (Exception $e) {

                $message = $e->getMessage();
                return $this->systemResponse($message, "500", "Error Occured");
            }
        } else {
            $message = "Incorect username OR Password ";
            return $this->systemResponse($message, 401, "INCORRECT CREDENTIALS");
        }
    }

    public function resetPassword()
    {
        //capture fields
        $id = $this->request->getPost("id");
        $email = $this->request->getPost("email");

        if (!$id) {
            $message = "User ID is required";
            return $this->systemResponse($message, 401, "Missing fields");
        }
        if (!$email) {
            $message = "Email is required";
            return $this->systemResponse($message, 401, "Missing fields");
        }
        $sql = "SELECT id FROM user WHERE id=:id AND email=:email ";

        $params = array(
            ":id" => $id,
            ":email" => $email
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "A reset link has been sent to your email if it exist in our system";
            return $this->systemResponse($message, "200", "Success");
        } catch (Exception $e) {

            $message = $e->getMessage();
            return $this->systemResponse($message, "500", "Error Occured");
        }

        //reset passsword
        $sql = "SELECT full_name,email FROM user WHERE id = :id";

        $params = array(":id" => $id);

        $user = $this->fetchOne($sql, $params);

        $full_name = $user->full_name;
        $email = $user->email;

        $token = md5("$user_id" . time());
        $msg = "Dear $full_name<br><br>Your account password at wakobo.com was changed successfully. To login <a href='www.wakobo.com/User/password/reset?token=$token'>Click Here </a> <br><br> <strong> Username: $email<br> New Password: $new_password</strong>";

        $this->sendMail($email, $full_name, "Password Reset", $msg);
    }

    public function processResetPassword()
    {

        $token = $this->request->get('token');
        $email = $this->request->getPost('email');
        $new_password = $this->request->getPost('new_password');

        // validate token,email, new_password
        if (!$token) {
            $message = "Token required";
            return $this->systemResponse($message, 401, "Missing field");
        }
        if (!$email) {
            $message = "Email required";
            return $this->systemResponse($message, 401, "Missing field");
        }
        if (!$new_password) {
            $message = "New Password required";
            return $this->systemResponse($message, 401, "missing field");
        }
        if (strlen($new_password) < 5) {
            $message = "New Password is too short";
            return $this->systemResponse($message, 401, "Missing field");
        }

        $sql = "SELECT * FROM user WHERE email=:email AND token = :token";

        $params = array(
            ":email" => $email,
            ":token" => $token
        );

        // update password and set reset_token = NULL
        $data = $this->fetchOne($sql, $params);

        if ($data) {

            $sql = "UPDATE user SET password=:password WHERE id=:id";

            $params = array(
                ":id" => $id,
                ":password" => md5($new_password)
            );
        }

        $sql = "SELECT full_name,email FROM user WHERE id = :id";

        $params = array(":id" => $id);

        $user = $this->fetchOne($sql, $params);

        $full_name = $user->full_name;
        $email = $user->email;

        $msg = "Dear $full_name<br><br>Your account password at wakobo.com was changed successfully. To login <a href='www.wakobo.com/login'>Click Here </a> <br><br> <strong> Username: $email<br> New Password: $new_password</strong>";

        $this->sendMail($email, $full_name, "New Password", $msg);
        $token = NULL;

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Password changed successfully";
            return $this->systemResponse($message, "200", "Success");
        } catch (Exception $e) {

            $message = $e->getMessage();
            return $this->systemResponse($message, "500", "Error Occured");
        }
    }

    public function login(){
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');

        // validate inputs - check if password and email was supplied
        if (!$email) {
            $message = "Email is required";
            return $this->systemResponse($message, 401, "Missing field");
        }
        if (!$password) {
            $message = "Password is required";
            return $this->systemResponse($message, 401, "Missing field");
        }

        $sql = "SELECT user.full_name,user.id,role.name,permission.module,permission.name FROM user 
                LEFT JOIN user_role ON user.id=user_role.user_id 
                LEFT JOIN role ON user_role.role_id=role.id
                LEFT JOIN role_permission ON user_role.role_id=role_permission.role_id
                LEFT JOIN permission ON role_permission.permission_id=permission.id
                WHERE user.email = :email AND user.password = :password";

        $params = array(
            ":email" => $email,
            ":password" => $password
        );
        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Image uploaded successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }


    }

    public function view()
    {

        // captures fields
        $id = $this->request->getPost("id");

        if (!$id) {
            $message = "User ID is required";
            return $this->systemResponse($message, 401, "Missing field");
        }
        $sql = "SELECT * FROM user WHERE id=:id";
        $params = array(":id" => $id);

        try {

            $dt = $this->rawSelect($sql, $params);
            return $this->systemResponse($dt, "200", "Success");
        } catch (Exception $e) {

            $message = $e->getMessage();
            return $this->systemResponse($message, "500", "Error Occured");
        }

    }
}
