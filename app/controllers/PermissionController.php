<?php
/**
 * Created by PhpStorm.
 * User: phil
 * Date: 29/08/2018
 * Time: 06:22
 */

class PermissionController extends ControllerBase
{

    public function create(){

        // captures fields
        $module = $this->request->getPost("module");
        $full_name = $this->request->getPost("full_name");

        // validate fields

        if(!$module){

            $message = "Module is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        if(!$full_name){

            $message = "Full name is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }


        $sql = "INSERT INTO permission (module,full_name,status,created) VALUE (:module,:full_name,1,now())";

        $params = array(
            ":module" => $module,
            ":full_name" => $full_name,
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "user created successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }



    }

}