<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/29/2018
 * Time: 6:22 AM
 */

class SubscriptionController extends ControllerBase
{

    public function create(){

        // captures fields
        $email = $this->request->getPost("email");

        if(!$email){

            $json = $this->request->getJsonRawBody();
            $email = isset($json->email) ? $json->email : false;
        }

        if(!$email){

            $message = "Email is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "INSERT IGNORE INTO subscription (email,created) VALUE (:email,now())";

        $params = array(
            ":email" => $email,
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "You have successfully subscribed to our Newsfeed";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }
    }

    public function delete(){

        // captures fields
        $email = $this->request->getPost("email");

        if(!$email){

            $message = "Email is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "DELETE from subscription WHERE email = :email LIMIT 1";
        $params = [':email'=>$email];

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "You have unsubscribed successfully from our Newsfeed";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }


}
