<?php
/**
 * Created by PhpStorm.
 * User: phil
 * Date: 29/08/2018
 * Time: 06:22
 */

class RolePermissionController extends ControllerBase
{

    public function create(){

        // captures fields
        $role_id = $this->request->getPost("role_id");
        $permission_id = $this->request->getPost("permission_id");

        // validate fields

        if(!$role_id){

            $message = "Role ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        if(!$permission_id){

            $message = "Permission ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }


        $sql = "INSERT INTO role_permission (role_id,permission_id,created) VALUE (:role_id,:permission_id, now())";

        $params = array(
            ":role_id" => $role_id,
            ":permission_id" => $permission_id,
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "user role created successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }



    }

}