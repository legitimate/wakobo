<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/29/2018
 * Time: 6:22 AM
 */

class ContactController extends ControllerBase
{

    public function create(){

        // captures fields
        $full_name = $this->request->getPost("full_name");
        $email = $this->request->getPost("email");
        $phone = $this->request->getPost("phone");

        if(!$full_name){

            $message = "Contact name is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }
        if(!$email){

            $message = "Email is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }
        if(!$phone){

            $message = "Phone no. is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }


        $sql = "INSERT INTO contact (full_name,email,phone,created) VALUE (:full_name,:email,:phone,now())";

        $params = array(
            ":full_name" => $full_name,
            ":email" => $email,
            ":phone" => $phone
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Language created successfully";
            return $this->systemResponse($dt,"200",$message);
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }
    }

    public function description(){

        // captures fields
        $contact_id = $this->request->getPost("contact_id");
        $language_id = $this->request->getPost("language_id");
        $description = $this->request->getPost("description");


        if(!$language_id){

            $message = "Language is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }
        if(!$contact_id){

            $message = "Contact is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        if(!$description){

            $message = "Contact description is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "INSERT INTO contact_description (contact_id,language_id,description,created) VALUE (:contact_id,:language_id,:description,NOW())";

        $params = array(
            ":contact_id" => $contact_id,
            ":language_id" => $language_id,
            ":description" => $description
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Contact description created successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }

    public function update(){

        // captures fields
        $id = $this->request->getPost("id");
        $full_name = $this->request->getPost("full_name");
        $email = $this->request->getPost("email");
        $phone = $this->request->getPost("phone");
        $description = $this->request->getPost("description");

        if(!$id){

            $message = "Contact ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $updates = array();
        $bindingParams = array();

        if($full_name){

            $updates[] = "full_name = :full_name";
            $bindingParams[':full_name'] = $full_name;
        }
        if($email){

            $updates[] = "email = :email";
            $bindingParams[':email'] = $email;
        }
        if($phone){

            $updates[] = "phone = :phone";
            $bindingParams[':phone'] = $phone;
        }
        if($description){

            $updates[] = "description = :description";
            $bindingParams[':description'] = $description;
        }

        if(count($updates) > 0){

            $sql = "UPDATE contact SET ".implode(',',$updates)." WHERE id = :id ";
            $bindingParams[':id'] = $id;

            try {

                $dt = $this->rawInsert($sql, $bindingParams);
                $message = "Contact updated successfully";
                return $this->systemResponse($message,"200","Success");
            }
            catch (Exception $e){

                $message = $e->getMessage();
                return $this->systemResponse($message,"500","Error Occured");
            }

        }

    }

    public function delete(){

        // captures fields
        $id = $this->request->getPost("id");

        if(!$id){

            $message = "Contact ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "DELETE from contact WHERE id = :id LIMIT 1";
        $params = [':id'=>$id];

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Language deleted successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }

    public function view(){

        // captures fields
        $id = $this->request->getPost("id");
        $language = $this->request->getPost("language");
        $json = $this->request->getJsonRawBody();

        if(!$id){
            $id = isset($json->id) ? $json->id : false;
        }

        if(!$language){
            $language = isset($json->language) ? $json->language : false;
        }

        $wheres = array();

        if($id){

            $wheres[] = "contact.id = $id";
            $params[] = [':id'=>$id];
        }

        if($language){

            $wheres[] = "language.short_name = '$language'";
            $params[] = [':short_name'=>$language];
        }

        if(count($wheres) > 0){

            $where = implode(' AND ',$wheres);
        }
        else {
            $where = 1;
        }

        $joins = array();
        $joins[] = "LEFT JOIN contact_description ON contact.id=contact_description.contact_id";
        $joins[] = "LEFT JOIN language ON contact_description.language_id=.language.id ";
        $join = implode(" ",$joins);

        $selects = array();
        $selects[] = "contact.id";
        $selects[] = "contact.email";
        $selects[] = "contact.phone";
        $selects[] = "contact.full_name";
        $selects[] = "contact_description.description";
        $selects[] = "language.short_name";
        $select = implode(",",$selects);

        $sql = "SELECT $select FROM contact $join WHERE $where";

        try {

            $dt = $this->rawSelect($sql);
            return $this->systemResponse($dt,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }

}
