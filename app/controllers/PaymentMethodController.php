<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/29/2018
 * Time: 6:22 AM
 */

class PaymentMethodController extends ControllerBase
{

    public function create(){

        // captures fields
        $full_name = $this->request->getPost("full_name");
        $description = $this->request->getPost("description");

        if(!$full_name){

            $message = "Name is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }
        if(!$description){

            $message = "Description is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }


        $sql = "INSERT INTO payment_method (full_name,description,created) VALUE (:full_name,:description,now())";

        $params = array(
            ":full_name" => $full_name,
            ":description" => $description,
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Payment created successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }
    }

    public function update(){

        // captures fields
        $id = $this->request->getPost("id");
        $full_name = $this->request->getPost("full_name");
        $description = $this->request->getPost("description");

        if(!$id){

            $message = "Payment ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $updates = array();
        $bindingParams = array();

        if($full_name){

            $updates[] = "full_name = :full_name";
            $bindingParams[':full_name'] = $full_name;
        }
        if($description){

            $updates[] = "description = :description";
            $bindingParams[':description'] = $description;
        }

        if(count($updates) > 0){

            $sql = "UPDATE payment_method SET ".implode(',',$updates)." WHERE id = :id ";
            $bindingParams[':id'] = $id;

            try {

                $dt = $this->rawInsert($sql, $bindingParams);
                $message = "Payment updated successfully";
                return $this->systemResponse($message,"200","Success");
            }
            catch (Exception $e){

                $message = $e->getMessage();
                return $this->systemResponse($message,"500","Error Occured");
            }

        }

    }

    public function delete(){

        // captures fields
        $id = $this->request->getPost("id");

        if(!$id){

            $message = "Payment ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "DELETE from payment_method WHERE id = :id LIMIT 1";
        $params = [':id'=>$id];

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Payment deleted successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }

    public function view(){

        // captures fields
        $id = $this->request->getPost("id");

        try {

            if(!$id){

                $sql = "SELECT * FROM payment_method ";
                $dt = $this->rawSelect($sql);
            }
            else {

                $sql = "SELECT * FROM payment_method WHERE id = :id ";
                $params = [':id'=>$id];
                $dt = $this->rawSelect($sql, $params);
            }

            return $this->systemResponse($dt,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }

}
