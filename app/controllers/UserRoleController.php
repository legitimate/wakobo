<?php
/**
 * Created by PhpStorm.
 * User: phil
 * Date: 29/08/2018
 * Time: 06:22
 */

class UserRoleController extends ControllerBase
{

    public function create(){

        // captures fields
        $user_id = $this->request->getPost("user_id");
        $role_id = $this->request->getPost("role_id");

        // validate fields

        if(!$user_id){

            $message = "User ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }
        if(!$role_id){

            $message = "Role ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }


        $sql = "INSERT INTO user_role (user_id,role_id,created) VALUE (:user_id,:role_id, now())";

        $params = array(
            ":user_id" => $user_id,
            ":role_id" => $role_id
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "user created successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }



    }

}