<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/29/2018
 * Time: 6:22 AM
 */

class MediaController extends ControllerBase
{

    public function create(){

        // check if request has file

        $allowed_extensions = array('png','jpg','jpeg','gif');

        if($this->request->hasFiles()){

            $files = $this->request->getUploadedFiles();
            $file = $files[0];

            $extension = $file->getExtension();
            $extension = strtolower($extension);

            if(!in_array($extension,$allowed_extensions)){

                $message = "Image format not allowed. File should be ".implode(',',$allowed_extensions);
                return $this->systemResponse($message,"401","Missing FIelds");
            }

            $upload_dir = $this->config->media->image;

            $new_name = $file->getName();
            // replace spaces by underscore
            $new_name = str_replace(' ','_',$new_name);

            // createfile path
            $source = $upload_dir.DIRECTORY_SEPARATOR.$new_name;

            // move file to the path
            if(!$file->moveTo($source)){

                $message = "Could not upload flag, make sure directory exists $source";
                return $this->systemResponse($message,"401","Missing FIelds");
            }
        }
        else {

            $message = "Language flag is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "INSERT INTO media (image,created) VALUE (:image,now())";

        $params = array(
            ":image" => $source,
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Image uploaded successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }
    }

    public function delete(){

        // captures fields
        $id = $this->request->getPost("id");

        if(!$id){

            $message = "Image ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "DELETE from media WHERE id = :id LIMIT 1";
        $params = [':id'=>$id];

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Image deleted successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }

}
