<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/29/2018
 * Time: 6:22 AM
 */

class BlogController extends ControllerBase
{

    public function create()
    {
        // captures fields
        $title = $this->request->getPost("title");
        $body = $this->request->getPost("body");
        $language_id = $this->request->getPost("language_id");
        $user_id = $this->request->getPost("user_id");
        $type = $this->request->getPost("type");
        $tag = $this->request->getPost("tag");
        $preview = $this->request->getPost("preview");

        if (!$title) {

            $message = "Title is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }
        if (!$body) {

            $message = "Body is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }
        if (!$language_id) {

            $message = "Language ID is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }
        if (!$user_id) {

            $message = "User ID is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }
        if (!$type) {

            $message = "Type is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        if ($type =="front_page" && !$preview) {

            $message = "Preview is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        if(!$preview){
            $preview = NULL;
        }

        $sql = "INSERT INTO blog (title,preview,body,language_id,user_id,type,tag,created) VALUE (:title,:preview,:body,:language_id,:user_id,:type,:tag,now())";

        $params = array(
            ":title" => $title,
            ":preview" => $preview,
            ":body" => $body,
            ":language_id" => $language_id,
            ":user_id" => $user_id,
            ":type" => $type,
            ":tag" => $tag,
        );

        try {

            $blog_id = $this->rawInsert($sql, $params);
            $message = "Blog created successfully";

            // check if request has file

            $allowed_extensions = explode(",", $this->config->allowed_media->blog);

            if ($this->request->hasFiles()) {

                $files = $this->request->getUploadedFiles();

                $this->info("Has files ".count($files));

                foreach ($files as $key => $file) {

                    $extension = $file->getExtension();
                    $extension = strtolower($extension);

                    if (!in_array($extension, $allowed_extensions)) {

                        $message = "Media format not allowed. File should be " . implode(',', $allowed_extensions);
                        return $this->systemResponse($message, "401", "Missing FIelds");
                    }

                    $upload_dir = $this->config->media->blog;

                    $route = $this->config->media->blog_url;
                    $time = time();

                    $new_name = "blog_" . $blog_id . "" . $key . "$time." . $extension;
                    // replace spaces by underscore
                    $new_name = str_replace(' ', '_', $new_name);

                    $route .= $new_name;

                    // createfile path
                    $source = $upload_dir . DIRECTORY_SEPARATOR . $new_name;

                    // move file to the path
                    if (!$file->moveTo($source)) {

                        $message = "Could not upload flag, make sure directory exists";
                        return $this->systemResponse($message, "401", "Missing FIelds");
                    }

                    // create media
                    $sql = "INSERT INTO media (image,image_url,extension,created) VALUE (:image,:image_url,:extension,now()) ";

                    $params = array(
                        ":image" => $source,
                        ":image_url" => $route,
                        ":extension" => $extension
                    );

                    $media_id = $this->rawInsert($sql, $params);


                    // create media blog
                    $sql = "INSERT INTO blog_media (blog_id,media_id,created) VALUE (:blog_id,:media_id,now()) ";

                    $params = array(
                        ":blog_id" => $blog_id,
                        ":media_id" => $media_id
                    );

                    $this->rawInsert($sql, $params);
                }
            }
            else {

                $this->info("Has no files ");

            }



            return $this->systemResponse($message, "200", "Success");


        } catch (Exception $e) {

            $message = $e->getMessage();
            return $this->systemResponse($message, "500", "Error Occured");
        }
    }

    public function update()
    {

        // captures fields
        $id = $this->request->getPost("id");
        $title = $this->request->getPost("title");
        $body = $this->request->getPost("body");
        $language_id = $this->request->getPost("language_id");
        $user_id = $this->request->getPost("user_id");
        $type = $this->request->getPost("type");
        $tag = $this->request->getPost("tag");

        if (!$id) {

            $message = "Blog ID is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        $updates = array();
        $bindingParams = array();

        if ($title) {

            $updates[] = "title = :title";
            $bindingParams[':title'] = $title;
        }
        if ($body) {

            $updates[] = "body = :body";
            $bindingParams[':body'] = $body;
        }
        if ($language_id) {

            $updates[] = "language_id = :langauge_id";
            $bindingParams[':language_id'] = $language_id;
        }
        if ($user_id) {

            $updates[] = "user_id = :user_id";
            $bindingParams[':user_id'] = $user_id;
        }
        if ($type) {

            $updates[] = "type = :type";
            $bindingParams[':type'] = $type;
        }
        if ($tag) {

            $updates[] = "tag = :tag";
            $bindingParams[':tag'] = $tag;
        }

        if (count($updates) > 0) {

            $sql = "UPDATE blog SET " . implode(',', $updates) . " WHERE id = :id ";
            $bindingParams[':id'] = $id;

            try {

                $dt = $this->rawInsert($sql, $bindingParams);
                $message = "Blog updated successfully";
                return $this->systemResponse($message, "200", "Success");
            } catch (Exception $e) {

                $message = $e->getMessage();
                return $this->systemResponse($message, "500", "Error Occured");
            }

        }

    }

    public function delete()
    {

        // captures fields
        $id = $this->request->getPost("id");

        if (!$id) {

            $message = "Blog ID is required";
            return $this->systemResponse($message, "401", "Missing FIelds");
        }

        $sql = "DELETE from blog WHERE id = :id LIMIT 1";
        $params = [':id' => $id];

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Blog deleted successfully";
            return $this->systemResponse($message, "200", "Success");
        } catch (Exception $e) {

            $message = $e->getMessage();
            return $this->systemResponse($message, "500", "Error Occured");
        }

    }

    public function view()
    {

        $json = $this->request->getJsonRawBody();
        $type = $this->request->getPost('type');
        $id = $this->request->getPost('id');
        $language = $this->request->getPost('language');
        $limit = $this->request->getPost('limit');

        $extraWhere = array();

        if (!$type) {
            $type = isset($json->type) ? $json->type : false;
        }

        if ($type) {
            $extraWhere[] = "blog.type = '$type'";
        }

        if (!$id) {

            $id = isset($json->id) ? $json->id : false;
        }

        if ($id) {

            $extraWhere[] = "blog.id = $id";
        }

        if (!$language) {
            $language = isset($json->language) ? $json->language : false;
        }

        if ($language) {
            $extraWhere[] = "language.short_name = '$language' ";
        }


        if (count($extraWhere) > 0) {

            $where = implode(" AND ", $extraWhere);
        } else {
            $where = 1;
        }

        $fields = array();
        $fields[] = "blog.id";
        $fields[] = "blog.title";
        $fields[] = "blog.preview";
        $fields[] = "blog.body";
        $fields[] = "blog.language_id";
        $fields[] = "user.full_name";
        $fields[] = "blog.user_id";
        $fields[] = "blog.type";
        $fields[] = "blog.tag";
        $fields[] = "blog.created";
        $fields[] = "media.image";
        $fields[] = "media.image_url";
        $fields[] = "media.extension";
        $fields[] = "blog.language_id";

        $joins = array();
        $joins[] = "INNER JOIN user ON blog.user_id = user.id";
        $joins[] = "INNER JOIN language ON blog.language_id = language.id";
        $joins[] = "LEFT JOIN blog_media ON blog.id = blog_media.blog_id";
        $joins[] = "LEFT JOIN media ON blog_media.media_id = media.id";

        $join = implode(" ", $joins);
        $field = implode(",", $fields);

        if(!$limit) {

            $sql = "SELECT $field FROM blog $join WHERE $where ORDER BY blog.id DESC ";
        }

        else  {

            $sql = "SELECT $field FROM blog $join WHERE $where ORDER BY blog.id DESC LIMIT $limit";
        }

        $data = $this->rawSelect($sql);

        $blogs = array();

        foreach ($data as $row) {

            $blog_id = $row['id'];

            $blog = isset($blogs[$blog_id]) ? $blogs[$blog_id] : array();

            if(!$blog) {
                $blog = array(
                    "title" => $row['title'],
                    "preview" => $row['preview'],
                    "body" => $row['body'],
                    "user_id" => $row['user_id'],
                    "type" => $row['type'],
                    "tag" => $row['tag'],
                    "created" => $row['created'],
                    "language_id" => $row['language_id'],
                    "full_name" => $row['full_name']
                );
            }

            $media = array(
                "image"         => $row['image'],
                "image_url"     => $row['image_url'],
                "extension"     => $row['extension']
            );

            $blog['media'][] = $media;

            $blogs[$blog_id] = $blog;

        }

        $res = array();

        foreach ($blogs as $key => $value){

            $value['media_count'] = isset($value['media']) ? sizeof($value['media']) : 0;
            $res[] = $value;

        }

        return $this->systemResponse($res,"200","OK");

    }
}
