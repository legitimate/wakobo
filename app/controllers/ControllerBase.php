<?php

use Firebase\JWT\JWT;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{

    /**
     * gets current datetime as string
     * @return false|string
     */

    public function getTime()
    {
        return date("Y-m-d H:i:s");
    }

    /**
     * perfom curl post
     * @param type $url
     * @param type $postData
     * @return array of response and http staus code
     */
    public function curl($url, $postData)
    {
        $this->info("posting to $url data " . json_encode($postData));
        $httpRequest = curl_init($url);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, json_encode($postData));
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: ' . 'application/json', 'Content-Length: ' . strlen(json_encode($postData))));
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($httpRequest, CURLOPT_HEADER, false);
        $resultData = curl_exec($httpRequest);
        $status = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE);
        curl_close($httpRequest);

        $response = array("response" => $resultData, "status" => $status);

        $this->info("received from $url " . json_encode($response));

        return $response;
    }

    /**
     * @param        $message
     * @param int $statusCode
     * @param string $statusDescription
     */

    public function systemResponse($message, $statusCode = 200, $statusDescription = 'success')
    {
        $message = array('data' => $message, 'status' => $statusCode);
        $this->info(json_encode($message));
        $this->response->setHeader("Content-Type", "application/json");
        $this->response->setHeader("Access-Control-Allow-Origin", "*");
        $this->response->setStatusCode($statusCode, $statusDescription);
        $this->response->setJsonContent($message);
        $this->response->send();
    }

    public function rawInsert($phql, $params = null) // update, delete & insert
    {
        try {
            $this->db->execute($phql, $params);
            $last_insert_id = $this->db->lastInsertId();
            return $last_insert_id;
        } catch (Exception $e) {
            $this->error("SQL " . $phql . " PARAMS " . json_encode($params) . " error " . $e->getMessage());
            throw $e;
        }
    }

    /**
     * executes SQL Select statement
     *
     * @param      $statement
     * @param null $params
     *
     * @return mixed
     * @throws \Exception
     */
    public function rawSelect($statement, $params = null)
    {
        try {
            $connection = $this->di->getShared("db");
            $success = $connection->query($statement, $params);
            $success->setFetchMode(Phalcon\Db::FETCH_ASSOC);
            $success = $success->fetchAll($success);
            return $success;
        } catch (Exception $e) {
            $this->error( "SQL " . $statement . " PARAMS " . json_encode($params) . " error " . $e->getMessage());
            throw $e;
        }
    }

    /**
     * @return bool|\Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    protected function isAuthenticated()
    {
        if ($this->session->has("user")) {
            return true;
        }
        return $this->response->redirect('login');
    }

    /**
     * Go Back from whence you came
     * @return type
     */
    protected function _redirectBack()
    {
        return $this->response->redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * executes SQL Select statement
     *
     * @param      $statement
     * @param null $params
     *
     * @return mixed
     * @throws \Exception
     */
    public function fetchOne($statement, $params = null, $mode = Phalcon\Db::FETCH_OBJ)
    {
        try {

            $connection = $this->di->getShared("db");
            $response = $connection->query($statement, $params);
            $response->setFetchMode($mode);
            $response = $response->fetchAll($response);
            if ($response && count($response) > 0) {

                return $response[0];
            }
            return false;
        } catch (Exception $e) {
            $this->error("SQL " . $statement . " PARAMS " . json_encode($params) . " error " . $e->getMessage());
            throw $e;
        }
    }

    /**
     * Logs an application message
     *
     * @param String $message Message to be logged
     * @param String $logLevel Logging level
     *
     * @return void
     */
    public function info($message = "")
    {
        $dir = $this->config->log->directory;
        $file = $this->config->log->info;

        $dir = rtrim($dir, '/');
        $file = rtrim($file);

        $logger = new FileAdapter($dir . '/' . $file);

        if (is_array($message) || is_object($message)) {
            $message = json_encode($message);
        }

        $logger->info($message);

    }

    /**
     * Logs an application message
     *
     * @param String $message Message to be logged
     * @param String $logLevel Logging level
     *
     * @return void
     */
    public function error($message = "")
    {
        $dir = $this->config->log->directory;
        $file = $this->config->log->error;

        $dir = rtrim($dir, '/');
        $file = rtrim($file);

        $logger = new FileAdapter($dir . '/' . $file);

        if (is_array($message) || is_object($message)) {
            $message = json_encode($message);
        }

        $logger->error($message);

    }

    /**
     * Logs an application message
     *
     * @param String $message Message to be logged
     * @param String $logLevel Logging level
     *
     * @return void
     */
    public function alert($message = "")
    {
        $dir = $this->config->log->directory;
        $file = $this->config->log->info;

        $dir = rtrim($dir, '/');
        $file = rtrim($file);

        $logger = new FileAdapter($dir . '/' . $file);

        if (is_array($message) || is_object($message)) {
            $message = json_encode($message);
        }

        $logger->alert($message);
    }

    /**
     * Logs an application message
     *
     * @param String $message Message to be logged
     * @param String $logLevel Logging level
     *
     * @return void
     */
    public function critical($message = "")
    {
        $dir = $this->config->log->directory;
        $file = $this->config->log->sms;

        $dir = rtrim($dir, '/');
        $file = rtrim($file);

        $logger = new FileAdapter($dir . '/' . $file);

        if (is_array($message) || is_object($message)) {
            $message = json_encode($message);
        }

        $logger->critical($message);
    }

    /**
     * Logs an application message
     *
     * @param String $message Message to be logged
     * @param String $logLevel Logging level
     *
     * @return void
     */
    public function debug($message = "")
    {
        $dir = $this->config->log->directory;
        $file = $this->config->log->info;

        $dir = rtrim($dir, '/');
        $file = rtrim($file);

        $logger = new FileAdapter($dir . '/' . $file);

        if (is_array($message) || is_object($message)) {
            $message = json_encode($message);
        }

        $logger->debug($message);
    }

    public function missingToken()
    {
        $data = array('data' => "you do not have permission to access the requested resource", 'status' => 403);
        $this->error(json_encode($data));
        return $this->systemResponse($data, 403, "Access Denied");
    }

    public function invalidToken()
    {
        $data = array('data' => "you do not have permission to access the requested resource, invalid token supplied", 'status' => 403);
        $this->error(json_encode($data));
        return $this->systemResponse($data, 403, "Access Denied");
    }

    public function accessDenied()
    {
        $this->error('you do not have permission to access the requested resource');

        return $this->systemResponse("you do not have permission to access the requested resource", 403, "Access Denied");
    }

    public function missingData($data = "required fields are missing")
    {
        $this->error($data);

        return $this->systemResponse($data, 421, 'MISSING FIELDS');
    }

    public function notFound($data = "resource not found")
    {
        $this->error($data);
        return $this->systemResponse($data, 404, 'NOT FOUND');
    }

    public function randomCode($length = 5, $type = 'integer')
    {
        //return 1234;

        $numeric = "23456789";

        $characters = "ABCDEFGHJKMNPQRSTUVWXYZ";

        if ($type == 'integer') {
            $characters = $numeric;
        } else if ($type == 'alphanumeric') {
            $characters .= $numeric;
        }

        $result = " ";

        for ($i = 0; $i < $length; $i++) {
            $result .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return trim(strtoupper($result));
    }

    /**
     * @param $token
     * @param User $user
     *
     * @return int
     */

    public function isValidUser($token, $user)
    {
        if (!is_null($user->deleted) || strlen($user->deleted) > 0 || $user->status == 0) {
            return false;
        }

        JWT::$leeway = 20;

        $key = $user->password;

        try {
            $decoded = JWT::decode($token, $key, array('HS256'));
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return 0;
        }
        if (isset($decoded->id) && $decoded->id == $user->id) {
            return 1;
        }

        return 0;
    }

    /**
     * @param \User $user
     *
     * @return string
     */

    public function issueUserToken($user)
    {
        $key = $user->password;
        $issuedAt = time();
        $notBefore = $issuedAt;             //Adding 10 seconds
        $expire = $notBefore + 60;            // Adding 60 seconds
        $serverName = gethostname(); // Retrieve the server name from config file

        $token = array(
            "iss" => $serverName,
            "iat" => $issuedAt,
            "nbf" => $notBefore,
            "id" => $user->id
        );

        $jwt = JWT::encode($token, $key);
        return $jwt;
    }

    public function verifyToken($token, $action = 0)
    {
        $key = "IMw2c3W5KWLFN1sBH1befeFocdWUs0Sd";

        $decoded = '';

        try {
            $decoded = JWT::decode($token, $key, array('HS256'));
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return $decoded;
        }
        if ($decoded->action == $action) {
            return $decoded;
        }
        if (isset($decoded->name) && isset($decoded->userId)) {
            return $decoded;
        }

        return;
    }

    public function verifyUser($key, $token)
    {
        $decoded = '';

        try {
            $decoded = JWT::decode($token, $key, array('HS256'));
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return $decoded;
        }
        if ($decoded->action == $action) {
            return $decoded;
        }
        if (isset($decoded->name) && isset($decoded->userId)) {
            return $decoded;
        }

        return;
    }

    /**
     * @param \User $user
     *
     * @return string
     */
    public function issueToken($user)
    {
        $key = $user->password;
        $issuedAt = time();
        $notBefore = $issuedAt + 1;             //Adding 10 seconds
        $expire = $notBefore + 60;            // Adding 60 seconds
        $serverName = gethostname(); // Retrieve the server name from config file

        $token = array(
            "iss" => $serverName,
            "iat" => $issuedAt,
            "nbf" => $notBefore,
            "id" => $user->id,
            "client_id" => 1
        );
        $jwt = JWT::encode($token, $key);
        return $jwt;
    }

    public function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}
