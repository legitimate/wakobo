<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/29/2018
 * Time: 6:22 AM
 */

class MessageController extends ControllerBase
{

    public function create(){

        // captures fields
        $full_name = $this->request->getPost("full_name");
        $email = $this->request->getPost("email");
        $text = $this->request->getPost("text");
        $json= $this->request->getJsonRawBody();

        if(!$full_name){
            $full_name = isset($json->full_name) ? $json->full_name : false;
        }

        if(!$email){
            $email = isset($json->email) ? $json->email : false;
        }

        if(!$text){
            $text = isset($json->text) ? $json->text : false;
        }

        if(!$full_name){

            $message = "FullName is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }
        if(!$email){

            $message = "Email is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }
        if(!$text){

            $message = "Message is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "INSERT INTO message (full_name,email,text,created) VALUE (:full_name,:email,:text,now())";

        $params = array(
            ":full_name" => $full_name,
            ":email" => $email,
            ":text" => $text,
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Message sent successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }
    }

    public function delete(){

        // captures fields
        $id = $this->request->getPost("id");

        if(!$id){

            $message = "Message ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "DELETE from message WHERE id = :id LIMIT 1";
        $params = [':id'=>$id];

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Message deleted successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }


}
