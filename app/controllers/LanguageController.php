<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/29/2018
 * Time: 6:22 AM
 */

class LanguageController extends ControllerBase
{

    public function create(){

        // captures fields
        $full_name = $this->request->getPost("full_name");
        $short_name  = $this->request->getPost("short_name");

        if(!$full_name){

            $message = "Language name is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        if(!$short_name){

            $message = "Language abbreviation name is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        // check if request has file

        $allowed_extensions = explode(",",$this->config->allowed_media->flag);

        if($this->request->hasFiles()){

            $files = $this->request->getUploadedFiles();
            $file = $files[0];

            $extension = $file->getExtension();
            $extension = strtolower($extension);

            if(!in_array($extension,$allowed_extensions)){

                $message = "Flag format not allowed. File should be ".implode(',',$allowed_extensions);
                return $this->systemResponse($message,"401","Missing FIelds");
            }

            $upload_dir = $this->config->media->flag;

            $route = $this->config->media->flag_url;

            //"/Users/phil/Desktop/sites/html/wakobo/media/flags/french.png"

            $new_name = $full_name.".$extension";
            // replace spaces by underscore
            $new_name = str_replace(' ','_',$new_name);

            $route .= $new_name;

            // createfile path
            $source = $upload_dir.DIRECTORY_SEPARATOR.$new_name;

            // move file to the path
            if(!$file->moveTo($source)){

                $message = "Could not upload flag, make sure directory exists";
                return $this->systemResponse($message,"401","Missing FIelds");
            }
        }
        else {

            $message = "Language flag is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "INSERT INTO language (full_name,short_name,flag,flag_url,created) VALUE (:fullname,:short_name,:flag,:flag_url,now())";

        $params = array(
            ":fullname" => $full_name,
            ":short_name" => $short_name,
            ":flag" => $source,
            ":flag_url" => $route
        );



        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Language created successfully";
            $language_file = $this->config->language->dir."$short_name.ini";

            if(!is_file($language_file)){
                //Some simple example content.
                $contents = '';
                //Save our content to the file.
                file_put_contents($language_file, $contents);
            }
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }
    }

    public function update(){

        // captures fields
        $full_name = $this->request->getPost("full_name");
        $id = $this->request->getPost("id");

        if(!$id){

            $message = "Language ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $updates = array();
        $bindingParams = array();

        if($full_name){

            $updates[] = "full_name = :fullname";
            $bindingParams[':fullname'] = $full_name;
        }

        // check if request has file

        $allowed_extensions = array('png','jpg','jpeg','gif');

        if($this->request->hasFiles()){

            $files = $this->request->getUploadedFiles();
            $file = $files[0];

            $extension = $file->getExtension();
            $extension = strtolower($extension);

            if(!in_array($extension,$allowed_extensions)){

                $message = "Flag format not allowed. File should be ".implode(',',$allowed_extensions);
                return $this->systemResponse($message,"401","Missing FIelds");
            }

            $upload_dir = $this->config->media->flag;

            $new_name = $full_name.".$extension";
            // replace spaces by underscore
            $new_name = str_replace(' ','_',$new_name);

            // createfile path
            $source = $upload_dir.DIRECTORY_SEPARATOR.$new_name;

            // move file to the path
            if(!$file->moveTo($source)){

                $message = "Could not upload flag, make sure directory exists";
                return $this->systemResponse($message,"401","Missing FIelds");
            }

            $updates[] = "flag = :flag";
            $bindingParams[':flag'] = $source;
        }

        if(count($updates) > 0){

            $sql = "UPDATE language SET ".implode(',',$updates)." WHERE id = :id ";
            $bindingParams[':id'] = $id;

            try {

                $dt = $this->rawInsert($sql, $bindingParams);
                $message = "Language updated successfully";
                return $this->systemResponse($message,"200","Success");
            }
            catch (Exception $e){

                $message = $e->getMessage();
                return $this->systemResponse($message,"500","Error Occured");
            }

        }

    }

    public function delete(){

        // captures fields
        $id = $this->request->getPost("id");

        if(!$id){

            $message = "Language ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "DELETE from language WHERE id = :id LIMIT 1";
        $params = [':id'=>$id];

        $sqlGet = "SELECt short_name from language WHERE id = :id LIMIT 1";

        try {

            $short_name = $this->fetchOne($sqlGet,$params);
            $short_name = $short_name->short_name;

            $dt = $this->rawInsert($sql, $params);
            $message = "Language deleted successfully";

            $language_file = $this->config->language->dir."$short_name.ini";

            if(!is_file($language_file)){
                unlink($language_file);
            }

            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }

    function getLocationInfoByIp(){

        $client  = @$_SERVER['HTTP_CLIENT_IP'];

        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];

        $remote  = @$_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP)){

            $ip = $client;

        }elseif(filter_var($forward, FILTER_VALIDATE_IP)){

            $ip = $forward;

        }else{

            $ip = $remote;

        }

        $result  = array('country'=>'', 'city'=>'','ip'=> $ip);

        $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));

        $result['ip_data'] = $ip_data;

        if($ip_data && $ip_data->geoplugin_countryName != null){

            $result->country = $ip_data->geoplugin_countryCode;

            $result->city = $ip_data->geoplugin_city;

        }

        return $result;

    }

    public function view(){

        $json = $this->request->getJsonRawBody();

        // captures fields
        $id = $this->request->getPost("id");

        // captures fields
        $short_name = $this->request->getPost("short_name");

        if(!$short_name){
            $short_name = isset($json->short_name) ? $json->short_name : false;
        }

        if($short_name){

            $language_file = $this->config->language->dir."$short_name.ini";
            $data = parse_ini_file($language_file,"1");
            //$data['county'] = $this->getLocationInfoByIp();
            return $this->systemResponse($data,"200","Success");
        }

        try {

            if(!$id){

                $sql = "SELECT * FROM language ";
                $dt = $this->rawSelect($sql);
            }
            else {

                $sql = "SELECT * FROM language WHERE id = :id ";
                $params = [':id'=>$id];
                $dt = $this->rawSelect($sql, $params);
            }

            foreach ($dt as $key=>$row){

                $row['abbr'] = "?l=".$row['short_name'];
                $dt[$key] = $row;
            }

            return $this->systemResponse($dt,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }

}
