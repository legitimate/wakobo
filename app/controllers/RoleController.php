<?php
/**
 * Created by PhpStorm.
 * User: phil
 * Date: 29/08/2018
 * Time: 06:22
 */

class RoleController extends ControllerBase
{

    public function create(){

        // captures fields
        $full_name = $this->request->getPost("full_name");

        // validate fields

        if(!$full_name){

            $message = "Name is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }


        $sql = "INSERT INTO role (full_name,status,created) VALUE (:fullname,1, now())";

        $params = array(
            ":fullname" => $full_name
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "user created successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }



    }

}