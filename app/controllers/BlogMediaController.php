<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/29/2018
 * Time: 6:22 AM
 */

class BlogMediaController extends ControllerBase
{

    public function create(){

        // captures fields
        $blog_id = $this->request->getPost("blog_id");
        $media_id = $this->request->getPost("media_id");

        if(!$blog_id){

            $message = "Blog ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }
        if(!$media_id){

            $message = "Media ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "INSERT INTO blog_media (blog_id,media_id,created) VALUE (:blog_id,:media_id,now())";

        $params = array(
            ":blog_id" => $blog_id,
            ":media_id" => $media_id,
        );

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Media created successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }
    }

    public function delete(){

        // captures fields
        $id = $this->request->getPost("id");

        if(!$id){

            $message = "BlogMedia ID is required";
            return $this->systemResponse($message,"401","Missing FIelds");
        }

        $sql = "DELETE from blog_media WHERE id = :id LIMIT 1";
        $params = [':id'=>$id];

        try {

            $dt = $this->rawInsert($sql, $params);
            $message = "Blog deleted successfully";
            return $this->systemResponse($message,"200","Success");
        }
        catch (Exception $e){

            $message = $e->getMessage();
            return $this->systemResponse($message,"500","Error Occured");
        }

    }


}
