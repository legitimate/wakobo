<?php

use Phalcon\Loader;

$loader = new Loader();

// Register some namespaces

$loader->registerDirs(
    [
    "app/controllers/",
     "app/models/"
    ]
);
$loader->register();
