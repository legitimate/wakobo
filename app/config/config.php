<?php

	defined('APP_PATH') || define('APP_PATH',realpath('.'));

	$configs = parse_ini_file("config.ini", true);

	$configs['application'] = [
		'controllersDir' => APP_PATH . '/app/controllers/',
		'modelsDir'      => APP_PATH . '/app/models/',
		'viewsDir'       => APP_PATH . '/app/views',
		'cacheDir'       => APP_PATH . '/app/cache/',
		'baseUri'        => preg_replace('/public([\/\\\\])index.php$/','',$_SERVER["PHP_SELF"]),
	];

	return new \Phalcon\Config($configs);