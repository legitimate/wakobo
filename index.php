<?php

use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

date_default_timezone_set('Africa/Nairobi');

//error_reporting(E_ALL);

define('APP_PATH',realpath(''));

/**
 * Read the configuration
 */
$config = include APP_PATH . "/app/config/config.php";

/**
 * Read auto-loader
 */
include APP_PATH . "/app/config/loader.php";

/**
 * Read services
 */
include APP_PATH . "/app/config/services.php";

/**
 * Read composer libraries
 */
include APP_PATH . "/vendor/autoload.php";

$app = new Micro($di);

$User = new MicroCollection();
$User->setPrefix('/User/');
$User->setHandler('UserController',true);
$User->post('add','create');
$User->post('status','changeStatus');
$User->post('password/change','changePassword');
$User->post('sendMail','sendMail');
$User->post('password/reset','resetPassword');
$User->post('password/processreset','processResetPassword');
$User->post('login','login');
$User->post('view','view');

$Role = new MicroCollection();
$Role->setPrefix('/Role/');
$Role->setHandler('RoleController',true);
$Role->post('add','create');

// add language API
$Language = new MicroCollection();
$Language->setPrefix('/Language/');
$Language->setHandler('LanguageController',true);
$Language->post('add','create');
$Language->post('edit','update');
$Language->post('view','view');
$Language->post('delete','delete');

//add permission API
$Permission = new MicroCollection();
$Permission->setPrefix('/Permission/');
$Permission->setHandler('PermissionController',true);
$Permission->post('add','create');

//add RolePermission API
$RolePermission = new MicroCollection();
$RolePermission->setPrefix('/RolePermission/');
$RolePermission->setHandler('RolePermissionController',true);
$RolePermission->post('add','create');

//add UserRole API
$UserRole = new MicroCollection();
$UserRole->setPrefix('/UserRole/');
$UserRole->setHandler('UserRoleController',true);
$UserRole->post('add','create');

//add Media API
$Media = new MicroCollection();
$Media->setPrefix('/Media/');
$Media->setHandler('MediaController',true);
$Media->post('add','create');
$Media->post('delete','delete');

//add Blog API
$Blog = new MicroCollection();
$Blog->setPrefix('/Blog/');
$Blog->setHandler('BlogController',true);
$Blog->post('add','create');
$Blog->post('edit','update');
$Blog->post('delete','delete');
$Blog->post('view','view');

//add BlogMedia API
$BlogMedia = new MicroCollection();
$BlogMedia->setPrefix('/BlogMedia/');
$BlogMedia->setHandler('BlogMediaController',true);
$BlogMedia->post('add','create');
$BlogMedia->post('delete','delete');

//add Subscription API
$Subscription = new MicroCollection();
$Subscription->setPrefix('/Subscription/');
$Subscription->setHandler('SubscriptionController',true);
$Subscription->post('add','create');
$Subscription->post('delete','delete');

//add Message API
$Message = new MicroCollection();
$Message->setPrefix('/Message/');
$Message->setHandler('MessageController',true);
$Message->post('add','create');
$Message->post('delete','delete');

//add Setting API
$Setting = new MicroCollection();
$Setting->setPrefix('/Setting/');
$Setting->setHandler('SettingController',true);
$Setting->post('add','create');
$Setting->post('edit','update');
$Setting->post('view','view');
$Setting->post('delete','delete');

// add contact API
$Contact = new MicroCollection();
$Contact->setPrefix('/Contact/');
$Contact->setHandler('ContactController',true);
$Contact->post('add','create');
$Contact->post('edit','update');
$Contact->post('view','view');
$Contact->post('delete','delete');
$Contact->post('description','description');


// add PaymentMethod API
$PaymentMethod = new MicroCollection();
$PaymentMethod->setPrefix('/PaymentMethod/');
$PaymentMethod->setHandler('PaymentMethodController',true);
$PaymentMethod->post('add','create');
$PaymentMethod->post('edit','update');
$PaymentMethod->post('view','view');
$PaymentMethod->post('delete','delete');


$app->mount($User);
$app->mount($Role);
$app->mount($Language);
$app->mount($Permission);
$app->mount($RolePermission);
$app->mount($UserRole);
$app->mount($Media);
$app->mount($Blog);
$app->mount($BlogMedia);
$app->mount($Subscription);
$app->mount($Message);
$app->mount($Setting);
$app->mount($Contact);
$app->mount($PaymentMethod);

try
{
    // Handle the request
    $response = $app->handle();
}
catch (\Exception $e)
{
    $res = new \stdClass();
    $res->code = "Error";
    $res->message = "An Error Ocurred. Reason:" . $e->getMessage();
    $res->data = [];

    header("Content-Type: text/plain;charset=utf-8");
    header('Access-Control-Allow-Origin:*');

    $phpSapiName = substr(php_sapi_name(), 0, 3);
    if ($phpSapiName == 'cgi' || $phpSapiName == 'fpm') {
        http_response_code($e->getCode() == 400 ? 400 : 404);
    } else {
        $protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0    ';
        http_response_code($e->getCode() == 400 ? 400 : 404);
    }

    echo json_encode($res);
}